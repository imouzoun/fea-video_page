(function () {
    window.app = window.app || {};

    app.UIManager = function ({ container, containerId, videoTemplate, videoTemplateId, paginationContainer, paginationContainerId }) {
        this.videosContainer = container || document.getElementById(containerId);
        this.videoTemplate = videoTemplate || document.getElementById(videoTemplateId);
        this.paginationContainer = paginationContainer || document.getElementById(paginationContainerId);
    };

    app.UIManager.prototype.render = function ({ videos, itemsPerPage }) {
        itemsPerPage = itemsPerPage || 10;

        this._currentPage = 1;
        this._itemsPerPage = itemsPerPage;
        this._videosElements = videos.map((video) => this._creteVideoContainer({ video }));

        this._renderPage();
    };

    app.UIManager.prototype._renderPage = function(){
        var from = (this._currentPage - 1) * this._itemsPerPage,
            to   = Math.min( (this._currentPage) * this._itemsPerPage, this._videosElements.length);
        
        var appendFunc = document.appendChild.bind(this.videosContainer);
        this.videosContainer.innerHTML = "";
        this._videosElements
            .slice(from, to)
            .map((fragment)=> document.importNode(fragment, true) )
            .forEach(appendFunc);

        var pagination = this._createPagination();

        this.paginationContainer.innerHTML = '';
        this.paginationContainer.appendChild(pagination);
    }

    app.UIManager.prototype._createPagination = function(){
        var totalItems      = this._videosElements.length,
            currentPage     = this._currentPage,
            itemsPerPage    = this._itemsPerPage;

        //TODO optiize
        var ul = document.createElement('ul');
        ul.classList.add('pager');

        var previous = document.createElement('li'),
            next = document.createElement('li');
        previous.innerHTML = '<a href="#">Previous</a></li>';
        next.innerHTML = '<a href="#">next</a></li>';

        if(totalItems > itemsPerPage){
            var hasNextPage = itemsPerPage * currentPage < totalItems;
            if(currentPage != 1){
                previous.getElementsByTagName('a')[0].addEventListener('click', (e) => {
                    e.preventDefault();
                    this._currentPage --;
                    this._renderPage();
                });
                ul.appendChild(previous);
            }
            if(hasNextPage){
                next.getElementsByTagName('a')[0].addEventListener('click', (e) => {
                    e.preventDefault();
                    this._currentPage ++;
                    this._renderPage();
                });
                ul.appendChild(next);
            }
        }

        return ul;
    }

    app.UIManager.prototype._creteVideoContainer = function ({ video }) {
        var templateContent = this.videoTemplate.content;

        templateContent.querySelector('.video').innerHTML = video.video;
        templateContent.querySelector('.title').innerHTML = video.title;
        templateContent.querySelector('.description').innerHTML = video.description;

        var clone = document.importNode(templateContent, true);
        
        return clone;
    };

    app.SearchManager = function ({ searchContainer, searchContainerId, vimeo }) {
        this.searchContainer = searchContainer || document.getElementById(searchContainerId);
        this.vimeo = vimeo;
        this._onChangeListener = [];
        this._initEventListelenrs();
    };

    app.SearchManager.prototype._filterVideos = function({searchText, moreThanTen = false}){
        return this.vimeo.data
            .map((item) => {
                let userTotalLikes = item.user.metadata.connections.likes.total;
                return {
                    title : item.name,
                    video : item.embed.html,
                    description : item.description || "",
                    userTotalLikes
                };
            })
            .filter((vid)=>{
                let idx = vid.description.toLowerCase().indexOf(searchText.toLowerCase());
                let ok = idx !== -1;
                if(moreThanTen){
                    ok = (ok && vid.userTotalLikes >= 10);
                }
                return ok;
            });
    };

    app.SearchManager.prototype._callListeners = function({videos, itemsPerPage}){
        this._onChangeListener.forEach((calB) => {
            calB.call(null, {videos, itemsPerPage});
        });
    };

    app.SearchManager.prototype.onChange = function(callBack){
        this._onChangeListener.push(callBack);
    };

    app.SearchManager.prototype._initEventListelenrs = function(){
        //TODO
    }


})();



document.addEventListener("DOMContentLoaded", function () {
    var vimeo = app.vimeo;

    var uiManager = new app.UIManager({
        containerId: 'videos',
        paginationContainerId : 'pagination',
        videoTemplateId : 'video-template',
        vimeo: app.vimeo
    });

    var searchManager = new app.SearchManager({
        searchContainerId: 'sidebar'
    });


    searchManager.onChange((videos, itemsPerPage) => {
        uiManager.render({videos, itemsPerPage});
    });


    window.d = {
        uiManager,
        searchManager
    };


});